﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Preferences;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.DAL.Preferences
{
    public class PreferencesManagerDAL
    {

        public void savePreferences(PreferencesBO preferences)
        {
            //Set data
            Properties.Settings.Default.DumpFolder  = preferences.BackupDirectory.FullURL;
            Properties.Settings.Default.SaveLog     = preferences.SaveLog;
            Properties.Settings.Default.NotifyTray  = preferences.UpdateOnTray;
            Properties.Settings.Default.ExclusionsDataSet = preferences.Exclusions.getRules();
            Console.WriteLine("Datatable saved");

            //Saving
            Properties.Settings.Default.Save();
        }

        public PreferencesBO loadPreferences()
        {
            //Loading
            PreferencesBO preferences = new PreferencesBO();

            //Set data
            try
            {
                preferences.BackupDirectory = new WSDirectoryInfoBO(Properties.Settings.Default.DumpFolder);
                preferences.Exclusions.setRules(Properties.Settings.Default.ExclusionsDataSet);
            }
            catch (Exception e)
            {
            }


            return (preferences);
        }
    }
}
