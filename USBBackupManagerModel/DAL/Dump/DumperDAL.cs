﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.File;
using USBBackupManagerModel.BO.Preferences.Exclusions;
using USBBackupManagerModel.Utils;

namespace USBBackupManagerModel.DAL.Dump
{
    public class DumperDAL
    {
        private LogManagerDAL logManager;

        private FileInfoBO logFile;
        private DirectoryInfoBO logFileDir;

        public DumperDAL()
        {
            this.logManager = new LogManagerDAL();
        }

        public void dumpDriveTo(DriveInfoBO sourceDrive, DirectoryInfoBO destinationDirectory, ExclusionsBO exclusions)
        {
            //Creates a folder with the drive ID
            string folderName = USBBackupManagerModel.Utils.General.convertToAValidPath(sourceDrive.DeviceRoot.UID + "_" + sourceDrive.ID);
            DirectoryInfoBO driveFolder = destinationDirectory.newFolder(@folderName);

            //Creates the txt log file
            try
            {
                string logFileName = Utils.General.convertToAValidPath(folderName + Utils.General.GetTimestamp() + ".txt");
                logFile = destinationDirectory.newFile(logFileName);
                logFileDir = destinationDirectory;
            }
            catch (Exception e)
            {
            }
            //Subscribes the logFile to the log manager
            this.logManager.addHandler(writeOnLog);
            
            //Dumps the selected drive
            this.logManager.addMessage("Backing " + sourceDrive.ID + " drive");
            this.dumpDrive(sourceDrive.DirectoryRoot, driveFolder, exclusions);
            this.logManager.addMessage("Backup of " + sourceDrive.ID + " drive complete");
        }

        public FileInfoBO getFileByNameAndDirectory(DirectoryInfoBO directory, string fileName)
        {
            FileInfoBO fileInfo = null;

            foreach (FileInfoBO f in directory.getFiles())
            {
                if (f.FullName == fileName)
                {
                    fileInfo = f;
                }
            }

            return (fileInfo);
        }

        public void writeFileToDirectory(FileInfoBO file, DirectoryInfoBO directory)
        {
            //directory.newFile(file);
        }

        private void dumpDrive(DirectoryInfoBO sourceDirectory, DirectoryInfoBO destinationDirectory, ExclusionsBO exclusions)
        {
            //Read every folder on the sourceDirectory
            foreach (DirectoryInfoBO directoryAux in sourceDirectory.getDirectories())
            {
                DirectoryInfoBO newDirectory = destinationDirectory.newFolder(directoryAux.Name);
                
                //Call recursively the function to copy all files
                dumpDrive(directoryAux, newDirectory, exclusions);
            }

            //Copy all files from the source directory to the destination one
            foreach (FileInfoBO file in sourceDirectory.getFiles())
            {
                //Check if the file should be copied
                if (exclusions.ShouldBeIncluded(file))
                {
                    try
                    {
                        byte[] data = file.loadData();
                        destinationDirectory.newFile(file.FullName, data);
                        this.logManager.addMessage(file.FullURL, 'O', file.Size); //Succesfully copied
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error creating file: " + e.Message.ToString());
                        this.logManager.addMessage(e.Message.ToString(), 'E'); //Error during copy
                    }
                }
                else
                {
                    this.logManager.addMessage(file.FullURL, 'X', file.Size); //Not included in copied files
                }
            }
        }

        private void writeOnLog(string message)
        {
            try
            {
                FileStream.WriteLine((WSDirectoryInfoBO)this.logFileDir, (WSDFileInfoBO)this.logFile, message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Log error: " + e.Message);
            }

        }
    }
}
