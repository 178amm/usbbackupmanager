﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using USBBackupManagerModel.Utils;

namespace USBBackupManagerModel.DAL
{
    public class LogManagerDAL
    {
        public delegate void NewMessageLogHandler(string message);
        private static NewMessageLogHandler newMessageBroadcast;

        public void addMessage(string message, char charCODE = ' ', ulong bytesSize = 0)
        {
            if (bytesSize == 0)
            {
                LogManagerDAL.newMessageBroadcast(getTimestamp() + " " +
                                                    charCODE + "\t" + 
                                                    "\t" +
                                                    message);
            }
            else
            {
                LogManagerDAL.newMessageBroadcast(getTimestamp() + " " +
                                                   charCODE + " " +
                                                   General.FormatBytes(bytesSize) + "\t" +
                                                   message);
            }
        }

        public void addHandler(NewMessageLogHandler handler)
        {
            LogManagerDAL.newMessageBroadcast += handler;
        }


        private string getTimestamp()
        {
           // return ("[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "]");
            return ("[" + DateTime.Now.ToLongTimeString() + "]");
        }
    }
}
