﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.DAL
{
    public class DeviceManagerDAL
    {
        public DeviceManagerDAL()
        {

        }

        public List<DeviceInfoBO> getConnectedDevices()
        {
            List<DeviceInfoBO> deviceList = new List<DeviceInfoBO>();

            WSDeviceInfoBO WSDeviceInfoAUX = new WSDeviceInfoBO(new DriveInfo("C"));

            //1
            //Get Windows Standar Devices (WSD)
            foreach (WSDeviceInfoBO d in WSDeviceInfoAUX.getConnectedDevices())
            {
                deviceList.Add(d);
            }

            WPDeviceInfoBO WPDeviceInfoAUX = new WPDeviceInfoBO("C", "c", "C");

            //2
            //Get Windows Portable Devices (WPD) 
            foreach (WPDeviceInfoBO d in WPDeviceInfoAUX.getConnectedDevices())
            {
                //(check inclued)
                if (!this.previouslyAddedOnWSDeviceList(d))
                {
                    deviceList.Add(d);
                }
            }

            return (deviceList);
        }




        //Ensures theres no duplicate drives between WPD Devices and Standard units (like USB drives detected from both dectectors)
        private bool previouslyAddedOnWSDeviceList(WPDeviceInfoBO device)
        {
            bool added = false;

            //Get all WSDevices
            WSDeviceInfoBO WSDeviceInfoAUX = new WSDeviceInfoBO(new DriveInfo("C"));

            for (int i = 0; !added && i < WSDeviceInfoAUX.getConnectedDevices().Count; i++)
            {
                if (device.UID == WSDeviceInfoAUX.getConnectedDevices()[i].UID) //Currently exists
                {
                    added = true;
                }
            }

            return (added);
        }

    }
}