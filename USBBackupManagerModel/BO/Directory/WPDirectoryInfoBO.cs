﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using PortableDeviceApiLib;
using PortableDeviceTypesLib;
using _tagpropertykey = PortableDeviceApiLib._tagpropertykey;
using IPortableDeviceKeyCollection = PortableDeviceApiLib.IPortableDeviceKeyCollection;
using IPortableDeviceValues = PortableDeviceApiLib.IPortableDeviceValues;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Directory
{
    public class WPDirectoryInfoBO : DirectoryInfoBO
    {

        public WPDirectoryInfoBO(WPDriveInfoBO driveRoot, string id, string name)
        {
            this.driveRoot = driveRoot;
            this.directoryURL = id;
            this.iD = id;
            this.name = name;
        }

        public override string Name
        {
            get
            {
                return base.Name;
            }
        }

        public override List<DirectoryInfoBO> getDirectories()
        {
            List<DirectoryInfoBO> directoryList = new List<DirectoryInfoBO>();
            PortableDeviceClass device = new PortableDeviceClass();

            //Conects to the device
            var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
            device.Open(this.driveRoot.DeviceRoot.ID, clientInfo);

            //Get the content
            IPortableDeviceContent content = null;
            device.Content(out content);

            // Get the properties of the object
            IPortableDeviceProperties properties;
            content.Properties(out properties);

            //Enumera los objetos
            IEnumPortableDeviceObjectIDs objectIds;
            content.EnumObjects(0, this.iD, null, out objectIds);

            //Los saca por pantalla
            uint fetched = 0;
            do
            {
                string objectID;

                objectIds.Next(1, out objectID, ref fetched);

                if (fetched > 0)
                {
                    WPDirectoryInfoBO newFolder = getFolderInfo(properties, objectID);
                    if (newFolder != null)
                    {
                        directoryList.Add(newFolder);
                    }
                }

            } while (fetched > 0);

            device.Close();

            return (directoryList);
        }

        public override List<FileInfoBO> getFiles()
        {
            List<FileInfoBO> fileList = new List<FileInfoBO>();
            PortableDeviceClass device = new PortableDeviceClass();

            //Conects to the device
            var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
            device.Open(this.driveRoot.DeviceRoot.ID, clientInfo);

            //Get the content
            IPortableDeviceContent content = null;
            device.Content(out content);

            // Get the properties of the object
            IPortableDeviceProperties properties;
            content.Properties(out properties);

            //Enumera los objetos
            IEnumPortableDeviceObjectIDs objectIds;
            content.EnumObjects(0, this.iD, null, out objectIds);

            //Los saca por pantalla
            uint fetched = 0;
            do
            {
                string objectID;

                objectIds.Next(1, out objectID, ref fetched);

                if (fetched > 0)
                {
                    FileInfoBO newFile = getFileInfo(properties, objectID);
                    if (newFile != null)
                    {
                        fileList.Add(newFile);
                    }
                }

            } while (fetched > 0);

            device.Close();

            return (fileList);
        }

        public override FileInfoBO newFile(string fileName, byte[] data)
        {
            throw new NotImplementedException();
        }

        public override DirectoryInfoBO newFolder(string folderName)
        {
            throw new NotImplementedException();
        }


        public override bool exists()
        {
            throw new NotImplementedException();
        }







        private WPDFileInfoBO getFileInfo(IPortableDeviceProperties properties, string objectID)
        {
            WPDFileInfoBO file = null;
            IPortableDeviceKeyCollection keys;
            properties.GetSupportedProperties(objectID, out keys);

            IPortableDeviceValues values;
            properties.GetValues(objectID, keys, out values);

            // Get the name of the object
            string name;
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                        0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 4;
            values.GetStringValue(property, out name);

            // Get the type of the object
            Guid contentType;
            property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                      0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 7;
            values.GetGuidValue(property, out contentType);

            // Get the file's size
            ulong fileSize;
            property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                    0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 11;
            values.GetUnsignedLargeIntegerValue(property, out fileSize);


            var folderType = new Guid(0x27E2E392, 0xA111, 0x48E0, 0xAB, 0x0C,
                                      0xE1, 0x77, 0x05, 0xA0, 0x5F, 0x85);

            var functionalType = new Guid(0x99ED0160, 0x17FF, 0x4C44, 0x9D, 0x98,
                              0x1D, 0x7A, 0x6F, 0x94, 0x19, 0x21);

            //If one of them is a folder, or a functionalType (no idea) , add them to the drive list
            if (contentType == folderType || contentType == functionalType)
            {
                //
            }
            else
            {
                file = new WPDFileInfoBO(this, properties, objectID);
            }


            return (file);
        }


        private WPDirectoryInfoBO getFolderInfo(IPortableDeviceProperties properties, string objectID)
        {
            WPDirectoryInfoBO folder = null;
            IPortableDeviceKeyCollection keys;
            properties.GetSupportedProperties(objectID, out keys);

            IPortableDeviceValues values;
            properties.GetValues(objectID, keys, out values);

            // Get the name of the object
            string name;
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                        0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 4;
            values.GetStringValue(property, out name);

            // Get the type of the object
            Guid contentType;
            property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                      0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 7;
            values.GetGuidValue(property, out contentType);

            var folderType = new Guid(0x27E2E392, 0xA111, 0x48E0, 0xAB, 0x0C,
                                      0xE1, 0x77, 0x05, 0xA0, 0x5F, 0x85);

            var functionalType = new Guid(0x99ED0160, 0x17FF, 0x4C44, 0x9D, 0x98,
                              0x1D, 0x7A, 0x6F, 0x94, 0x19, 0x21);

            //If one of them is a folder, or a functionalType (no idea) , add them to the drive list
            if (contentType == folderType || contentType == functionalType)
            {
                folder = new WPDirectoryInfoBO((WPDriveInfoBO) this.driveRoot, objectID, name);
            }

            return (folder);
        }

    }
}
