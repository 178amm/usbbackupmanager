﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.File;
using USBBackupManagerModel.BO.Drive;

namespace USBBackupManagerModel.BO.Directory
{
    public abstract class DirectoryInfoBO
    {
        protected DriveInfoBO driveRoot;
        protected string directoryURL;
        protected string name;
        protected string iD;

        public string ID
        {
            set { iD = value; }
            get { return iD; }
        }

        public virtual string Name
        {
            get { return name; }
        }

        public string DirectoryURL
        {
            get { return directoryURL; }
            set { directoryURL = value; }
        }

        public virtual string FullURL
        {
            get { return System.IO.Path.GetFullPath(driveRoot.Root + DirectoryURL); }
        }

        public DriveInfoBO DriveRoot
        {
            get { return driveRoot; }
        }

        public abstract List<FileInfoBO> getFiles();
        public abstract List<DirectoryInfoBO> getDirectories();
        public abstract DirectoryInfoBO newFolder(string folderName);
        public abstract FileInfoBO newFile(string fileName, byte[] data = null);
        public abstract bool exists();
    }
}
