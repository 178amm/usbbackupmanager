﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Directory
{
    public class WSDirectoryInfoBO : DirectoryInfoBO
    {
        private DirectoryInfo auxDirectoryInfo;

        public WSDirectoryInfoBO(WSDriveInfoBO driveRoot, string directoryName = "")
        {
            this.driveRoot = driveRoot;
            this.directoryURL = directoryName;

            string auxURL = driveRoot.Root + directoryName;

            this.auxDirectoryInfo = new DirectoryInfo(driveRoot.Root + directoryName);
        }

        public WSDirectoryInfoBO(string fullURL)
        {
            string fullDirectoryURL = Path.GetFullPath(fullURL);

            if (fullDirectoryURL != null)
            {
                string driveName = fullDirectoryURL.Substring(0, 2);

                foreach (DriveInfo d in DriveInfo.GetDrives())
                {
                    if (driveName == d.Name.Substring(0,2))
                    {
                        //DriveInfo founded
                        this.driveRoot = new WSDriveInfoBO(new Device.WSDeviceInfoBO(d), d);
                        this.directoryURL = fullDirectoryURL.Substring(2);

                        this.auxDirectoryInfo = new DirectoryInfo(fullDirectoryURL);
                    }
                }
            }
        }

        public override string Name
        {
            get
            {
                return (auxDirectoryInfo.Name);
            }
        }

        public override List<FileInfoBO> getFiles()
        {
            List<FileInfoBO> fileList = new List<FileInfoBO>();

            try
            {
                foreach (FileInfo f in auxDirectoryInfo.GetFiles())
                {
                        WSDFileInfoBO fileInfo = new WSDFileInfoBO(this,f);

                        fileList.Add(fileInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting files: " + e.Message.ToString());
            }


            return (fileList);
        }

        public override List<DirectoryInfoBO> getDirectories()
        {
            List<DirectoryInfoBO> directoryList = new List<DirectoryInfoBO>();

            try
            {
                foreach (DirectoryInfo d in this.auxDirectoryInfo.GetDirectories())
                {
                    WSDirectoryInfoBO directoryInfo = new WSDirectoryInfoBO((WSDriveInfoBO)this.driveRoot, d.FullName.Remove(0, 3));
                    directoryList.Add(directoryInfo);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting directories: " + e.Message.ToString());
            }

            return (directoryList);
        }

        public override FileInfoBO newFile(string fileName, byte[] data = null)
        {
            FileInfoBO file = USBBackupManagerModel.BO.File.FileStream.NewFile(this, fileName, data);

            return (file);
        }

        public override DirectoryInfoBO newFolder(string folderName)
        {
            DirectoryInfoBO newDirectory = null;
            string newURL = Path.GetFullPath(this.FullURL + "/" + folderName);

            try
            {
                newDirectory = new WSDirectoryInfoBO(newURL);
                if (!System.IO.Directory.Exists(newURL))
                {
                    System.IO.Directory.CreateDirectory(newURL);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error creating folder: " + e.Message.ToString());
            }

            return (newDirectory);
        }

        public override bool exists()
        {
            return(this.auxDirectoryInfo.Exists);
        }

    }
}
