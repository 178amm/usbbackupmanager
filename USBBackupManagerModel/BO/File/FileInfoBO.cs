﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.File
{
    public abstract class FileInfoBO
    {
        protected string name;
        protected string fullName;
        protected string fullURL;

        public string FullURL
        {
            get { return fullURL; }
        }

        protected ulong size;
        protected string iD;
        protected string format;

        public string Format
        {
            get { return format; }
        }

        public string ID
        {
            get { return iD; }
        }

        public ulong Size
        {
            get { return size; }
        }

        public string FullName
        {
            get { return fullName; }
        }

        public FileInfoBO()
        {

        }

        public abstract byte[] loadData();
        public abstract bool exists();
    }
}
