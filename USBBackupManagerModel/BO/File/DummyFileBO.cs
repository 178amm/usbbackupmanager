﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBBackupManagerModel.BO.File
{
    public class DummyFileBO : FileInfoBO
    {
        public DummyFileBO(ulong size, string fullName, string format)
        {
            this.size = size;
            this.fullName = fullName;
            this.format = format;
        }

        public override bool exists()
        {
            throw new NotImplementedException();
        }

        public override byte[] loadData()
        {
            throw new NotImplementedException();
        }
    }
}
