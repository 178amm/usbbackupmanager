﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using USBBackupManagerModel.BO.File;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.File
{
    public static class FileStream
    {
        //File reading method for a Windows Standard Device
        public static WSDFileInfoBO ReadFile(WSDirectoryInfoBO directoryInfo, string fileName)
        {
            //The file it returns
            WSDFileInfoBO file = new WSDFileInfoBO(directoryInfo);


            //Create the needed URL
            string fullURL = directoryInfo.FullURL + "/" + fileName;

            try
            {
                System.IO.FileStream fileStream = new System.IO.FileStream(fullURL, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                //Set data
                //file.Data = new byte[fileStream.Length];
                //fileStream.Read(file.Data, 0, (int)fileStream.Length);
                fileStream.Close();

                //Set name
                //file.FullName = fileName;
            }
            catch (Exception e)
            {
            
                Console.WriteLine("Error de lectura: " + e.Message.ToString());
            }

            return (file);
        }

        //File writing method for a Windows Standard Device
        public static bool WriteFile(WSDirectoryInfoBO directoryInfo, WSDFileInfoBO file)
        {
            //Return true if the fille has succesfully been copied
            bool error = false;

            //Create the needed URL
            string fullURL = Path.GetFullPath(directoryInfo.FullURL + "/" + file.FullName);

            try
            {
                System.IO.FileStream fileStream = new System.IO.FileStream(fullURL, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                //fileStream.Write(file.Data, 0, file.Data.Length);
                fileStream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error de escritura: " + e.Message.ToString());
                error = true;
            }

            return (error);
        }

        //Insert a line on a plain text file
        public static void WriteLine(WSDirectoryInfoBO directoryInfo, WSDFileInfoBO file, string message)
        {
            //Create the needed URL
            string fullURL = directoryInfo.FullURL + "/" + file.FullName;

            try
            {
                using (System.IO.FileStream fileStream = new System.IO.FileStream(fullURL, System.IO.FileMode.Append, System.IO.FileAccess.Write))

                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fileStream))
                {
                    sw.WriteLine(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error writing line: " + e.Message.ToString());
            }
        }

        //Create a new file
        public static WSDFileInfoBO NewFile(WSDirectoryInfoBO directoryInfo, string fileName, byte[] data = null)
        {
            WSDFileInfoBO file = null;

            //Create the needed URL
            string fullURL = Path.GetFullPath(directoryInfo.FullURL + "/" + fileName);

            System.IO.FileStream fileStream = new System.IO.FileStream(fullURL, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

            if (data == null)
            {
                fileStream.Write(new byte[0], 0, 0);
            }
            else
            {
                fileStream.Write(data, 0, data.Length);
            }

            fileStream.Close();

            //Instantiates variable
            FileInfo faux = new FileInfo(fullURL);

            file = new WSDFileInfoBO(directoryInfo, faux);

            return (file);
        }

    }
}
