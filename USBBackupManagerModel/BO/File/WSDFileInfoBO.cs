﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.File
{
    public class WSDFileInfoBO : FileInfoBO
    {
        WSDirectoryInfoBO directory;

        public WSDFileInfoBO(WSDirectoryInfoBO directory, FileInfo fileInfo = null)
        {
            this.directory = directory;

            if (fileInfo != null)
            {
                //this.iD = ...
                this.fullName = fileInfo.Name;
                this.format = this.fullName.Split('.').Last();
                if (this.format == null)
                    this.format ="";
                this.size = Convert.ToUInt64(fileInfo.Length);
                this.fullURL = directory.FullURL + this.fullName;
            }
        }

        public override bool exists()
        {
            throw new NotImplementedException();
        }

        public override byte[] loadData()
        {
            //Create the needed URL
            string fullURL = Path.GetFullPath(directory.FullURL + "/" + this.FullName);

            System.IO.FileStream fileStream = new System.IO.FileStream(fullURL, System.IO.FileMode.Open, System.IO.FileAccess.Read);

            //Set data
            byte[] data = new byte[fileStream.Length];

            try
            {
                fileStream.Read(data, 0, (int)fileStream.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error de lectura: " + e.Message.ToString());
            }

            fileStream.Close();

            return (data);
        }
    }
}
