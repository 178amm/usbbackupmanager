﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using PortableDeviceApiLib;
using PortableDeviceTypesLib;
using _tagpropertykey = PortableDeviceApiLib._tagpropertykey;
using IPortableDeviceKeyCollection = PortableDeviceApiLib.IPortableDeviceKeyCollection;
using IPortableDeviceValues = PortableDeviceApiLib.IPortableDeviceValues;

using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.File
{
    public class WPDFileInfoBO : FileInfoBO
    {
        private WPDirectoryInfoBO directory;

        public WPDFileInfoBO(WPDirectoryInfoBO directory, IPortableDeviceProperties properties = null, string objectID = null)
        {
            this.directory = directory;

            if (objectID != null && properties != null)
            {
                //Set properties
                this.iD = objectID;
                this.fullName   = this.getFileName(properties, objectID);
                this.size       = this.getFileSize(properties, objectID);
                this.fullURL    = this.getFullURL(properties, objectID);
                this.format = this.fullName.Split('.').Last();
                if (this.format == null)
                    this.format = "";
            }
        }

        public override bool exists()
        {
            throw new NotImplementedException();
        }

        public override byte[] loadData()
        {
            PortableDeviceClass device = new PortableDeviceClass();

            //Conect to the device
            var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
            device.Open(directory.DriveRoot.DeviceRoot.ID, clientInfo);

            //Transfer the content from the device
            IPortableDeviceContent content;
            device.Content(out content);

            IPortableDeviceResources resources;
            content.Transfer(out resources);

            PortableDeviceApiLib.IStream wpdStream;
            uint optimalTransferSize = 0;

            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xE81E79BE, 0x34F0, 0x41BF, 0xB5, 0x3F, 0xF1, 0xA0, 0x6A, 0xE8, 0x78, 0x42);
            property.pid = 0;

            resources.GetStream(this.iD, ref property, 0, ref optimalTransferSize, out wpdStream);

            System.Runtime.InteropServices.ComTypes.IStream sourceStream = (System.Runtime.InteropServices.ComTypes.IStream)wpdStream;

            byte[] data = new byte[this.Size];

            try
            {
                Console.WriteLine("Start reading...");
                unsafe
                {
                    int readSize = Convert.ToInt32(this.Size);
                    int bytesRead;
                    do
                    {
                        sourceStream.Read(data, readSize , new IntPtr(&bytesRead));
                    } while (bytesRead > 0);
                }

                Console.WriteLine("Finish reading");

                device.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error de lectura: " + e.Message.ToString());
            }

            return (data);
        }


        private string getFullURL(IPortableDeviceProperties properties, string objectID)
        {
            string fullURL = "";
            
            while (objectID != "DEVICE" && properties != null && objectID != null)
            {
                IPortableDeviceKeyCollection keys;
                properties.GetSupportedProperties(objectID, out keys);

                IPortableDeviceValues values;
                properties.GetValues(objectID, keys, out values);

                // Identify the property to retrieve
                var property = new _tagpropertykey();
                property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                            0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
                property.pid = 3; //WPD_OBJECT_PARENT_ID

                // Retrieve the parent objectID
                string WPD_OBJECT_PARENT_ID;
                values.GetStringValue(ref property, out WPD_OBJECT_PARENT_ID);

                // Retrieve the name of the object
                string WPD_OBJECT_NAME;
                property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                            0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
                property.pid = 4; //WPD_OBJECT_NAME
                values.GetStringValue(property, out WPD_OBJECT_NAME);

                //Add the parent name to the URL
                fullURL = "/" + WPD_OBJECT_NAME + fullURL;

                //Switch the parent ID for continuing the loop
                objectID = WPD_OBJECT_PARENT_ID;
            }

            return (fullURL);
        }

        private string getFileName(IPortableDeviceProperties properties, string objectID)
        {
            IPortableDeviceKeyCollection keys;
            properties.GetSupportedProperties(objectID, out keys);

            IPortableDeviceValues values;
            properties.GetValues(objectID, keys, out values);

            // Get the name of the object
            string name;
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                        0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 4;
            values.GetStringValue(property, out name);

            return (name);
        }

        private ulong getFileSize(IPortableDeviceProperties properties, string objectID)
        {
            IPortableDeviceKeyCollection keys;
            properties.GetSupportedProperties(objectID, out keys);

            IPortableDeviceValues values;
            properties.GetValues(objectID, keys, out values);

            // Get the file's size
            ulong fileSize;
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                    0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 11;
            values.GetUnsignedLargeIntegerValue(property, out fileSize);

            return (fileSize);
        }
    }
}
