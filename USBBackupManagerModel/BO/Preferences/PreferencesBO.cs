﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using USBBackupManagerModel.BO.Preferences.Exclusions;

using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.Preferences
{
    public class PreferencesBO
    {
        //Preferences
        private DirectoryInfoBO backupDirectory;
        private ExclusionsBO exclusions;

        public PreferencesBO()
        {
            exclusions = new ExclusionsBO();
        }

        public ExclusionsBO Exclusions
        {
            get { return exclusions; }
            set { exclusions = value; }
        }

        public DirectoryInfoBO BackupDirectory
        {
            get { return backupDirectory; }
            set { backupDirectory = value; }
        }

        bool saveLog;

        public bool SaveLog
        {
            get { return saveLog; }
            set { saveLog = value; }
        }

        bool updateOnTray;

        public bool UpdateOnTray
        {
            get { return updateOnTray; }
            set { updateOnTray = value; }
        }
    }
}
