﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    class ConditionContainsName : ICondition
    {
        public ConditionContainsName(string name)
        {
            this.ConditionValue = name;
        }

        public override bool Evaluate(FileInfoBO fileInfo)
        {
            string fileName = fileInfo.FullName;
            bool contains = base.Evaluate(fileInfo);

            if (!contains)
            {
                if (fileName.Contains(this.ConditionValue))
                {
                    contains = true;
                }
            }

            return (contains);
        }
    }
}
