﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    class ConditionFormat : ICondition
    {
        public ConditionFormat(string format)
        {
            this.ConditionValue = format;
        }

        public override bool Evaluate(FileInfoBO fileInfo)
        {
            string format = fileInfo.Format;
            bool meetFormat = base.Evaluate(fileInfo);

            if (!meetFormat)
            {
                if (format.ToLower() == this.ConditionValue.ToLower())
                {
                    meetFormat = true;
                }
            }

            return (meetFormat);
        }
    }
}
