﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    class ConditionMinSize : ICondition
    {
        public ConditionMinSize(string minSize)
        {
            this.ConditionValue = minSize;
        }

        public override bool Evaluate(FileInfoBO fileInfo)
        {
            string fileLength = fileInfo.Size.ToString();
            bool meetMinSize = base.Evaluate(fileInfo);

            if (!meetMinSize)
            {
                try
                {
                    if (Convert.ToInt64(fileLength) > Convert.ToInt64(this.ConditionValue))
                    {
                        meetMinSize = true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("No puede convertirse el tamaño del archivo: " + e.ToString());
                }
            }

            return (meetMinSize);
        }
    }
}
