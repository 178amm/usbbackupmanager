﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    class ConditionFolder : ICondition
    {
        public ConditionFolder(string folderCondition)
        {
            this.ConditionValue = folderCondition;
        }

        public override bool Evaluate(FileInfoBO fileInfo)
        {
            string directoryURL = fileInfo.FullURL;
            bool contains = base.Evaluate(fileInfo);

            if (!contains)
            {
                if (directoryURL.Contains(this.ConditionValue))
                {
                    contains = true;
                }
            }

            return (contains);
        }
    }
}
