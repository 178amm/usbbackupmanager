﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    class ConditionMaxSize : ICondition
    {
        public ConditionMaxSize(string maxSize)
        {
            this.ConditionValue = maxSize;
        }

        public override bool Evaluate(FileInfoBO fileInfo)
        {
            string fileLength = fileInfo.Size.ToString();
            bool meetMaxSize = base.Evaluate(fileInfo);

            if (!meetMaxSize)
            {
                try
                {
                    if (Convert.ToInt64(fileLength) < Convert.ToInt64(this.ConditionValue))
                    {
                        meetMaxSize = true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("No puede convertirse el tamaño del archivo: " + e.ToString());
                }
            }

            return (meetMaxSize);
        }
    }
}
