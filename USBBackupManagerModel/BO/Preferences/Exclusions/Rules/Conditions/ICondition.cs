﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions
{
    public abstract class ICondition
    {
        protected string conditionValue;

        public string ConditionValue
        {
            get { return conditionValue; }
            set { conditionValue = value; }
        }

        public virtual bool Evaluate(FileInfoBO fileInfo)
        {
            if (ConditionValue == "" || ConditionValue == null)
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }
    }
}
