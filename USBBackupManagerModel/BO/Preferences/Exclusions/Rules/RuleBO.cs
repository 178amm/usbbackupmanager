﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using USBBackupManagerModel.BO.File;
using USBBackupManagerModel.BO.Preferences.Exclusions.Rules.Conditions;

namespace USBBackupManagerModel.BO.Preferences.Exclusions.Rules
{
    public class RuleBO
    {
        private List<ICondition> conditions;

        public List<ICondition> Conditions
        {
            get { return conditions; }
        }
        private bool exclude;

        public bool Exclude
        {
            get { return exclude; }

        }
        private bool copyAnyways;

        public bool CopyAnyways
        {
            get { return copyAnyways; }
        }

        public XmlElement getXMLElement()
        {
            XmlDocument xDoc = new XmlDocument();
            XmlElement element = xDoc.CreateElement("RULE");

            element.SetAttribute("copyAnyways", this.copyAnyways.ToString());
            element.SetAttribute("exclude", this.exclude.ToString());
            element.SetAttribute("maxSize", this.Conditions[0].ConditionValue);
            element.SetAttribute("minSize", this.Conditions[1].ConditionValue);
            element.SetAttribute("format", this.Conditions[2].ConditionValue);
            element.SetAttribute("directoryURL", this.Conditions[3].ConditionValue);
            element.SetAttribute("name", this.Conditions[4].ConditionValue);
            Console.WriteLine("ELEMENT: " + element.OuterXml);
            return (element);
        }

        public RuleBO(XmlElement element)
        {
            this.copyAnyways = Convert.ToBoolean(element.GetAttribute("copyAnyways"));
            this.exclude = Convert.ToBoolean(element.GetAttribute("exclude"));

            //this.conditions.Clear();
            this.conditions = new List<ICondition>{
                new ConditionMaxSize(element.GetAttribute("maxSize")), 
                new ConditionMinSize(element.GetAttribute("minSize")), 
                new ConditionFormat(element.GetAttribute("format")),
                new ConditionFolder(element.GetAttribute("directoryURL")),
                new ConditionContainsName(element.GetAttribute("name"))
            };
        }

        public RuleBO(bool copyAnyways, bool exclude, string maxSize, string minSize, string format, string directoryURL, string name)
        {
            this.copyAnyways = copyAnyways;
            this.exclude = exclude;

            this.conditions = new List<ICondition>{
                new ConditionMaxSize(maxSize), 
                new ConditionMinSize(minSize), 
                new ConditionFormat(format),
                new ConditionFolder(directoryURL),
                new ConditionContainsName(name)
            };
        }

        public bool Evaluate(FileInfoBO fileInfo)
        {
            bool pass = true;

            for (int i=0; i<this.Conditions.Count && pass == true; i++)
            {
                if (!this.Conditions[i].Evaluate(fileInfo))
                {
                    pass = false;
                }
            }
            //Console.WriteLine("All conditions: " + pass);
            return (pass);
        }
    }
}
