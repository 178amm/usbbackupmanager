﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;

using USBBackupManagerModel.BO.Preferences.Exclusions.Rules;
using USBBackupManagerModel.BO.File;

namespace USBBackupManagerModel.BO.Preferences.Exclusions
{
    public class ExclusionsBO
    {
        private List<RuleBO> rules;

        public List<RuleBO> Rules
        {
            get { return rules; }
        }

        public ExclusionsBO()
        {
            this.rules = new List<RuleBO>();
        }

        public void setRules(string xml)
        {
            //XML to ruleList
            XmlDocument xmlRules = new XmlDocument();
            xmlRules.LoadXml(xml);

            XmlNodeList xmlRulesList = xmlRules.GetElementsByTagName("RULE");

            foreach (XmlElement element in xmlRulesList)
            {
                this.rules.Add(new RuleBO(element));
            }
        }

        public string getRules()
        {
            XmlDocument xmlRules = new XmlDocument();
            XmlElement rootNode = xmlRules.CreateElement("ROOT");
            xmlRules.AppendChild(rootNode);

            foreach (RuleBO rule in rules)
            {
                //necessary for crossing XmlDocument contexts
                XmlNode importNode = rootNode.OwnerDocument.ImportNode(rule.getXMLElement(), true);
                xmlRules.DocumentElement.AppendChild(importNode);
            }

            return (xmlRules.OuterXml);
        }

        public void addRule(RuleBO newRule)
        {
            this.Rules.Add(newRule);
        }

        public bool ShouldBeIncluded(FileInfoBO fileInfo)
        {
            bool included = false;

            //Copy Anyways Evaluation
            bool copyAnyways = false;
            for (int i = 0; i < rules.Count && copyAnyways == false; i++)
            {
                if (rules[i].Evaluate(fileInfo))
                {
                    copyAnyways = rules[i].CopyAnyways;
                }
            }
            if (copyAnyways == false)
            {
                //Included Evaluation
                for (int e = 0; e < rules.Count && included == false; e++)
                {
                    if (rules[e].Evaluate(fileInfo))
                    {
                        if (rules[e].Exclude == false)
                        {
                            included = true;
                        }
                    }
                }
                //Excluded Evaluation
                for (int j = 0; j < rules.Count && included == true; j++)
                {
                    if (rules[j].Exclude)
                    {
                        if (rules[j].Evaluate(fileInfo))
                        {
                            included = false;
                        }
                    }
                }
            }
            else
            {
                included = true;
            }

            return (included);
        }
    }
}
