﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using PortableDeviceApiLib;
using PortableDeviceTypesLib;
using _tagpropertykey = PortableDeviceApiLib._tagpropertykey;
using IPortableDeviceValues = PortableDeviceApiLib.IPortableDeviceValues;
using IPortableDeviceKeyCollection = PortableDeviceApiLib.IPortableDeviceKeyCollection;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;


namespace USBBackupManagerModel.BO.Device
{
    public class WPDeviceInfoBO : DeviceInfoBO
    {
        private readonly PortableDeviceManager deviceManager;
        private readonly PortableDeviceClass device;

        public PortableDeviceClass Device
        {
            get { return device; }
        } 


        public WPDeviceInfoBO(string id, string name, string uid)
        {
            this.iD = id;
            this.uID = uid;
            this.name = name;
            this.deviceManager = new PortableDeviceManager();
            this.device = new PortableDeviceClass();
        }

        public override List<DriveInfoBO> getAssociatedDrives()
        {
            List<DriveInfoBO> driveList = new List<DriveInfoBO>();
            try
            {
                //Conects to the device
                var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
                this.Device.Open(this.ID, clientInfo);

                //Get the content
                IPortableDeviceContent content = null;
                this.Device.Content(out content);

                // Get the properties of the object
                IPortableDeviceProperties properties2;
                content.Properties(out properties2);

                //Enum. all objects
                IEnumPortableDeviceObjectIDs objectIds;
                content.EnumObjects(0, "DEVICE", null, out objectIds);

                //Adds them to the list
                uint fetched = 0;
                do
                {
                    string objectId;

                    objectIds.Next(1, out objectId, ref fetched);

                    if (fetched > 0)
                    {
                        WPDriveInfoBO newDrive = getDriveInfoBO(properties2, objectId);

                        if (newDrive != null)
                            driveList.Add(newDrive);
                    }

                } while (fetched > 0);

                device.Close();
            }
            catch (Exception e)
            {
            }

            return (driveList);
        }

        public override List<DeviceInfoBO> getConnectedDevices()
        {
            List<DeviceInfoBO> deviceList = new List<DeviceInfoBO>();
            try
            {
                this.deviceManager.RefreshDeviceList();

                // Determine how many WPD devices are connected
                uint numberOfDevices = 1;
                string[] deviceIDS;
                this.deviceManager.GetDevices(null, ref numberOfDevices);

                if (numberOfDevices == 0)
                {
                    deviceIDS = (new string[0]);
                }

                deviceIDS = new string[numberOfDevices];

                deviceManager.GetDevices(ref deviceIDS[0], ref numberOfDevices);

                foreach (var deviceID in deviceIDS)
                {
                    string name = getWPDNameFromDeviceID(deviceID);
                    string serialNumber = getWPDSerialNumberFromDeviceID(deviceID);

                    deviceList.Add(new WPDeviceInfoBO(deviceID, name, serialNumber));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception listing WPDevices: " + e.Message);
            }

            return (deviceList);
        }

        private string getWPDSerialNumberFromDeviceID(string deviceID)
        {
            //Conects to the device
            var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
            this.Device.Open(deviceID, clientInfo);

            // Retrieve the properties of the device
            IPortableDeviceContent content;
            IPortableDeviceProperties properties;
            this.Device.Content(out content);
            content.Properties(out properties);

            // Retrieve the values for the properties
            IPortableDeviceValues propertyValues;
            properties.GetValues("DEVICE", null, out propertyValues);

            // Identify the property to retrieve
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0x26D4979A, 0xE643, 0x4626, 0x9E, 0x2B,
                                        0x73, 0x6D, 0xC0, 0xC9, 0x2F, 0xDC);
            property.pid = 9;

            // Retrieve the friendly name
            string propertyValue;
            propertyValues.GetStringValue(ref property, out propertyValue);

            device.Close();

            return (propertyValue);
        }

        private string getWPDNameFromDeviceID(string deviceID)
        {
            //Conects to the device
            var clientInfo = (IPortableDeviceValues)new PortableDeviceValuesClass();
            this.Device.Open(deviceID, clientInfo);

            // Retrieve the properties of the device
            IPortableDeviceContent content;
            IPortableDeviceProperties properties;
            this.Device.Content(out content);
            content.Properties(out properties);

            // Retrieve the values for the properties
            IPortableDeviceValues propertyValues;
            properties.GetValues("DEVICE", null, out propertyValues);

            // Identify the property to retrieve
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0x26D4979A, 0xE643, 0x4626, 0x9E, 0x2B,
                                        0x73, 0x6D, 0xC0, 0xC9, 0x2F, 0xDC);
            property.pid = 12;

            // Retrieve the friendly name
            string propertyValue;
            propertyValues.GetStringValue(ref property, out propertyValue);

            device.Close();

            return (propertyValue);
        }


        private WPDriveInfoBO getDriveInfoBO(IPortableDeviceProperties properties, string objectId)
        {
            WPDriveInfoBO drive = null;
            IPortableDeviceKeyCollection keys;
            properties.GetSupportedProperties(objectId, out keys);

            IPortableDeviceValues values;
            properties.GetValues(objectId, keys, out values);

            // Get the name of the object
            string name;
            var property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                        0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 4;
            values.GetStringValue(property, out name);

            // Get the type of the object
            Guid contentType;
            property = new _tagpropertykey();
            property.fmtid = new Guid(0xEF6B490D, 0x5CD8, 0x437A, 0xAF, 0xFC,
                                      0xDA, 0x8B, 0x60, 0xEE, 0x4A, 0x3C);
            property.pid = 7;
            values.GetGuidValue(property, out contentType);

            var folderType = new Guid(0x27E2E392, 0xA111, 0x48E0, 0xAB, 0x0C,
                                      0xE1, 0x77, 0x05, 0xA0, 0x5F, 0x85);

            var functionalType = new Guid(0x99ED0160, 0x17FF, 0x4C44, 0x9D, 0x98,
                              0x1D, 0x7A, 0x6F, 0x94, 0x19, 0x21);

            //If one of them is a folder, or a functionalType (no idea) , add them to the drive list
            if (contentType == folderType || contentType == functionalType)
            {
                drive = new WPDriveInfoBO((WPDeviceInfoBO)this, name, objectId);
            }

            return (drive);
        }


    }
}
