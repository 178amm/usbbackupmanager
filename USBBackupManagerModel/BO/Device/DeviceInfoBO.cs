﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Drive;

namespace USBBackupManagerModel.BO.Device
{
    public abstract class DeviceInfoBO
    {
        protected string uID;   //UNIQUE identifier (Drive's Serial Number)
        protected string iD;    //Identifier only at runtime
        protected string name;

        public string UID
        {
            get { return uID; }
        }

        public string Name
        {
            get { return name; }
        }

        public string ID
        {
            get { return iD; }
        }

        public abstract List<DriveInfoBO> getAssociatedDrives();
        public abstract List<DeviceInfoBO> getConnectedDevices();
    }
}
