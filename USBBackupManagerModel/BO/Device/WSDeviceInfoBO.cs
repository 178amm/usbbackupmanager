﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;

namespace USBBackupManagerModel.BO.Device
{
    public class WSDeviceInfoBO : DeviceInfoBO
    {
        private DriveInfo driveInfo;

        public WSDeviceInfoBO(string ID, string name)
        {
            this.iD = ID;
            this.name = name;
            this.uID = this.getWSDeviceUID(this.iD);
        }

        public WSDeviceInfoBO(DriveInfo driveInfo)
        {
            //Set the drive info and creates an unique runtime ID
            this.driveInfo = driveInfo;
            //this.iD = driveInfo.RootDirectory.ToString();
            this.iD = driveInfo.RootDirectory + this.name; //@TODO el cambio de arriba puede causar problemas

            //Set the name
            try
            {
                if (driveInfo.VolumeLabel == "")
                {
                    this.name = "NO_NAME";
                }
                else
                {
                    this.name = driveInfo.VolumeLabel;
                }
            }
            catch
            {
                this.name = "UNKNOWN_NAME";
            }

            //Set the UID, hardware identifier
            this.uID = this.getWSDeviceUID(this.iD);
        }

        public override List<DriveInfoBO> getAssociatedDrives()
        {
            List<DriveInfoBO> driveList = new List<DriveInfoBO>();

            driveList.Add(new WSDriveInfoBO(this, driveInfo));

            return (driveList);
        }

        public override List<DeviceInfoBO> getConnectedDevices()
        {
            List<DeviceInfoBO> deviceList = new List<DeviceInfoBO>();

            //Search for Windows Standar Devices (External & internal drives)
            foreach (DriveInfo driveInf in DriveInfo.GetDrives())
            {
                deviceList.Add(new WSDeviceInfoBO(driveInf));
            }

            return (deviceList);
        }

        
        private string getWSDeviceUID(string driveLetter) //driveLetter follows the format "C:\"
        {
            string uid = "nullUID";

            //Query for retrieving the partition using the drive assigned letter on the volume
            ManagementObjectSearcher ms = new ManagementObjectSearcher("ASSOCIATORS OF {Win32_LogicalDisk.DeviceID='" + driveLetter.Substring(0, 2) + "'} WHERE AssocClass = Win32_LogicalDiskToPartition");

            foreach (ManagementObject mo in ms.Get())
            {
                //Query for retrieving the physical disk drive related with the partition
                ManagementObjectSearcher ms2 = new ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + mo["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition");

                foreach (ManagementObject mo2 in ms2.Get())
                {
                    //In this case we use the drive serial number to create a unique identifier
                    uid = mo2["SerialNumber"].ToString();
                }
            }

            return (uid);
        }
    }
}
