﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.Drive
{
    public class WPDriveInfoBO : DriveInfoBO
    {
        public WPDriveInfoBO(WPDeviceInfoBO rootDevice, string name, string id)
        {
            this.root = name;
            this.iD = id;
            this.deviceRoot = rootDevice;
        }

        public override DirectoryInfoBO DirectoryRoot
        {
            get
            {
                return (new WPDirectoryInfoBO(this, this.iD, this.root));
            }
        }
    }
}
