﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.Drive
{
    public class WSDriveInfoBO : DriveInfoBO
    {
        public WSDriveInfoBO(WSDeviceInfoBO deviceRoot, DriveInfo driveInfo)
        {
            this.root = driveInfo.RootDirectory.FullName;
            this.iD = driveInfo.RootDirectory.FullName; //TODO Modificar uid
            this.deviceRoot = deviceRoot;
        }

        public override DirectoryInfoBO DirectoryRoot
        {
            get
            {
                return(new WSDirectoryInfoBO(this));
            }
        }

    }
}
