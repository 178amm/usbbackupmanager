﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;

namespace USBBackupManagerModel.BO.Drive
{
    public abstract class DriveInfoBO
    {
        protected DeviceInfoBO deviceRoot;
        protected DirectoryInfoBO directoryRoot;
        protected string root;
        protected string iD;

        public virtual DirectoryInfoBO DirectoryRoot
        {
            get { return directoryRoot; }
            set { directoryRoot = value;}
        }

        public string Root
        {
            get { return root; }
        }

        public DeviceInfoBO DeviceRoot
        {
            get { return deviceRoot; }
        }

        public string ID
        {
            get { return iD; }
        }

    }
}
