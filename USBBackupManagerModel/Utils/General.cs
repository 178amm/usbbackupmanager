﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace USBBackupManagerModel.Utils
{
    public static class General
    {
        public static string convertToAValidPath(string path)
        {
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                path = path.Replace(c, '_');
            }

            return (path);
        }

        // ...
        /// <summary>Formats bytes as a display friendly string</summary>
        /// <param name="aBytes">The number of bytes we want a string representation of </param>
        /// <returns>UI friendly string</returns>
        public static string FormatBytes(ulong aBytes)
        {
            const int lScale = 1024;
            string[] lStrings = new string[] { Properties.Resources.Terabytes, Properties.Resources.Gigabytes, Properties.Resources.Megabytes, Properties.Resources.Kilobytes, Properties.Resources.Bytes };
            ulong lCap = (ulong)Math.Pow(lScale, lStrings.Length - 1);

            for (int i = 0, j = lStrings.Length; i < j; ++i)
            {
                if (aBytes <= lCap)
                {
                    lCap /= lScale;
                    continue;
                }

                string lStr = lStrings[i];
                decimal lValue = decimal.Divide(aBytes, lCap);

                if (i == j - 1)
                    return string.Format(lStr, decimal.Divide(aBytes, lCap).ToString("N0"));
                else
                    return string.Format(lStr, decimal.Divide(aBytes, lCap).ToString("N", Thread.CurrentThread.CurrentCulture.NumberFormat));
            }

            return string.Format(Properties.Resources.Bytes, aBytes);
        }

        public static string GetTimestamp()
        {
            return ("[" + DateTime.Now.ToLongTimeString() + "]");
        }
    }
}
