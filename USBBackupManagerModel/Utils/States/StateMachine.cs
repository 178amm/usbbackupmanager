﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBBackupManagerModel.Utils.States
{
    public class StateMachine
    {
        

        private List<State> states;
        private State currentState;

        public StateMachine(List<State> states)
        {
            //Init. stuff
            if (states != null)
            {
                this.states = states;

                if (states.Count > 0)
                {
                    this.currentState = State.INITIAL_STATE;
                    this.states[0].addTrigger(State.INITIAL_STATE);
                }
            }
        }

        public bool changeState(State state)
        {
            if (states.Contains(state) && state.changeState(currentState))
            {
                this.currentState = state;
                return (true);
            }
            else
            {
                return (false);
            }
        }
    }
}
