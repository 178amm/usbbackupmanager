﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USBBackupManagerModel.Utils.States
{
    public class State
    {
        public delegate void OnStateChange();

        private static State iNITIAL_STATE = new State("INITIAL", null);

        public static State INITIAL_STATE
        {
            get { return iNITIAL_STATE; }
        }

        private String name;

        public String Name
        {
            get { return name; }
        }

        private OnStateChange onChange;
        private List<State> triggers;


        public OnStateChange OnChange
        {
            get { return onChange; }
            set { onChange = value; }
        }

        public State(String stateName, OnStateChange onChange)
        {
            //Init. stuff
            this.triggers = new List<State>();
            this.name = stateName;
            this.onChange = onChange;
        }

        public void addTrigger(State stateTrigger)
        {
            this.triggers.Add(stateTrigger);
        }

        public bool changeState(State currentState)
        {
            if (this.triggers.Contains(currentState))
            {
                if (this.OnChange != null)
                {
                    this.OnChange();
                }
                return (true);
            }
            else
            {
                return (false);
            }
        }

    }
}
