﻿namespace USBBackupManager
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exclusionsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.liveLogListBox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DrivesComboBox = new System.Windows.Forms.ComboBox();
            this.GoButton = new System.Windows.Forms.Button();
            this.AutoBackupModeCheckBox = new System.Windows.Forms.CheckBox();
            this.AbortButton = new System.Windows.Forms.Button();
            this.RefreshDrivesButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(736, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editionToolStripMenuItem
            // 
            this.editionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferencesToolStripMenuItem,
            this.exclusionsToolStripMenuItem1});
            this.editionToolStripMenuItem.Name = "editionToolStripMenuItem";
            this.editionToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.editionToolStripMenuItem.Text = "Preferences";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.preferencesToolStripMenuItem.Text = "General";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // exclusionsToolStripMenuItem1
            // 
            this.exclusionsToolStripMenuItem1.Name = "exclusionsToolStripMenuItem1";
            this.exclusionsToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.exclusionsToolStripMenuItem1.Text = "Exclusions";
            this.exclusionsToolStripMenuItem1.Click += new System.EventHandler(this.exclusionsToolStripMenuItem1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to USB Backup Manager";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Backup Now";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // liveLogListBox
            // 
            this.liveLogListBox.FormattingEnabled = true;
            this.liveLogListBox.Location = new System.Drawing.Point(288, 75);
            this.liveLogListBox.Name = "liveLogListBox";
            this.liveLogListBox.Size = new System.Drawing.Size(430, 147);
            this.liveLogListBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(288, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Live Log";
            // 
            // DrivesComboBox
            // 
            this.DrivesComboBox.FormattingEnabled = true;
            this.DrivesComboBox.Location = new System.Drawing.Point(15, 202);
            this.DrivesComboBox.Name = "DrivesComboBox";
            this.DrivesComboBox.Size = new System.Drawing.Size(204, 21);
            this.DrivesComboBox.TabIndex = 5;
            this.DrivesComboBox.SelectedIndexChanged += new System.EventHandler(this.DrivesComboBox_SelectedIndexChanged);
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(225, 200);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(48, 23);
            this.GoButton.TabIndex = 6;
            this.GoButton.Text = "Go!";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // AutoBackupModeCheckBox
            // 
            this.AutoBackupModeCheckBox.AutoSize = true;
            this.AutoBackupModeCheckBox.Location = new System.Drawing.Point(519, 231);
            this.AutoBackupModeCheckBox.Name = "AutoBackupModeCheckBox";
            this.AutoBackupModeCheckBox.Size = new System.Drawing.Size(118, 17);
            this.AutoBackupModeCheckBox.TabIndex = 8;
            this.AutoBackupModeCheckBox.Text = "Auto Backup Mode";
            this.AutoBackupModeCheckBox.UseVisualStyleBackColor = true;
            this.AutoBackupModeCheckBox.CheckedChanged += new System.EventHandler(this.AutoBackupModeCheckBox_CheckedChanged);
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(643, 227);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(75, 23);
            this.AbortButton.TabIndex = 9;
            this.AbortButton.Text = "Abort";
            this.AbortButton.UseVisualStyleBackColor = true;
            // 
            // RefreshDrivesButton
            // 
            this.RefreshDrivesButton.Location = new System.Drawing.Point(174, 227);
            this.RefreshDrivesButton.Name = "RefreshDrivesButton";
            this.RefreshDrivesButton.Size = new System.Drawing.Size(99, 23);
            this.RefreshDrivesButton.TabIndex = 10;
            this.RefreshDrivesButton.Text = "Refresh Drives";
            this.RefreshDrivesButton.UseVisualStyleBackColor = true;
            this.RefreshDrivesButton.Click += new System.EventHandler(this.RefreshDrivesButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 264);
            this.Controls.Add(this.RefreshDrivesButton);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.AutoBackupModeCheckBox);
            this.Controls.Add(this.GoButton);
            this.Controls.Add(this.DrivesComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.liveLogListBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "USB Backup Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.onClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox liveLogListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox DrivesComboBox;
        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.CheckBox AutoBackupModeCheckBox;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Button RefreshDrivesButton;
        private System.Windows.Forms.ToolStripMenuItem exclusionsToolStripMenuItem1;
    }
}

