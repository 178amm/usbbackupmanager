﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.DAL;
using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.File;


namespace USBBackupManager.BAL
{
    class DeviceManagerBAL
    {
        //Other BAL 
        private LogManagerBAL logManager;

        public DeviceManagerBAL()
        {
            this.logManager = new LogManagerBAL();
        }

        public List<DeviceInfoBO> getConnectedDevices()
        {
            List<DeviceInfoBO> connectedDevices = new List<DeviceInfoBO>();
            DeviceManagerDAL deviceManager = new DeviceManagerDAL();

            foreach (DeviceInfoBO device in deviceManager.getConnectedDevices())
            {
                connectedDevices.Add(device);
            }

            return(connectedDevices);
        }

        public List<DriveInfoBO> getConnectedDrives()
        {
            List<DriveInfoBO> connectedDrives = new List<DriveInfoBO>();
            DeviceManagerDAL deviceManager = new DeviceManagerDAL();

            foreach (DeviceInfoBO device in deviceManager.getConnectedDevices())
            {
                foreach (DriveInfoBO drive in device.getAssociatedDrives())
                {
                    connectedDrives.Add(drive);
                }
            }

            return (connectedDrives);
        }

        public DriveInfoBO getDriveByRootName(string root)
        {
            List<DriveInfoBO> connectedDrives = this.getConnectedDrives();

            foreach (DriveInfoBO drive in connectedDrives)
            {
                if (drive.Root == root)
                {
                    return (drive);
                }
            }

            return (null);
        }
    }
}
