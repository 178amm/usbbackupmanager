﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.BO.Preferences;
using USBBackupManagerModel.DAL.Preferences;

namespace USBBackupManager.BAL.Preferences
{
    public class PreferencesManagerBAL
    {
        public void savePreferences(PreferencesBO preferences)
        {
            PreferencesManagerDAL preferencesManager = new PreferencesManagerDAL();
            preferencesManager.savePreferences(preferences);
        }

        public PreferencesBO loadPreferences()
        {
            PreferencesManagerDAL preferencesManager = new PreferencesManagerDAL();
            PreferencesBO preferences = preferencesManager.loadPreferences();

            return (preferences);
        }
    }
}
