﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.DAL;
using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.File;

using USBBackupManagerModel.DAL.Dump;
using USBBackupManagerModel.BO.Preferences.Exclusions;

namespace USBBackupManager.BAL.Dump
{
    public class DumperBAL
    {
        public DumperBAL()
        {

        }

        public void dumpDriveTo(DriveInfoBO sourceDrive, DirectoryInfoBO directoryDestination, ExclusionsBO exclusions)
        {
            DumperDAL dumper = new DumperDAL();

            dumper.dumpDriveTo(sourceDrive, directoryDestination, exclusions);
        }

        public FileInfoBO getFileByNameAndDirectory(DirectoryInfoBO directory, string fileName)
        {
            DumperDAL dumper = new DumperDAL();

            return (dumper.getFileByNameAndDirectory(directory, fileName));
        }

        public void writeFileToDirectory(FileInfoBO file, DirectoryInfoBO directory)
        {
            DumperDAL dumper = new DumperDAL();

            dumper.writeFileToDirectory(file, directory);
        }
    }
}
