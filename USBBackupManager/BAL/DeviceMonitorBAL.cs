﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

using USBBackupManagerModel.DAL;
using USBBackupManagerModel.BO.Device;


namespace USBBackupManager.BAL
{
    class DeviceMonitorBAL
    {
        //Other BAL 
        private LogManagerBAL logManager;

        private const int REFRESH_TIME = 1000;
        public delegate void NewDeviceDetectedEventHandler(DeviceInfoBO detectedDevice);

        private Timer searcherTimer;
        private List<DeviceInfoBO> lastDetectedDevices;
        private NewDeviceDetectedEventHandler newDeviceDetectedEvent;

        internal NewDeviceDetectedEventHandler NewDeviceDetected
        {
            get { return newDeviceDetectedEvent; }
            set { newDeviceDetectedEvent = value; }
        }
       
        public DeviceMonitorBAL()
        {
            this.logManager = new LogManagerBAL();
            this.searcherTimer = new Timer(REFRESH_TIME);
            this.searcherTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            GC.KeepAlive(searcherTimer);
        }

        public void Start()
        {
            DeviceManagerBAL deviceManager = new DeviceManagerBAL();

            this.searcherTimer.Start();
            this.lastDetectedDevices = deviceManager.getConnectedDevices();
        }

        public void Stop()
        {
            this.searcherTimer.Stop();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            this.SearchForNewDevices();
        }

        private void SearchForNewDevices()
        {
            DeviceManagerBAL deviceManager = new DeviceManagerBAL();

            List<DeviceInfoBO> newDetectedDevices = deviceManager.getConnectedDevices();

            Console.WriteLine("Searching...");

            //Search new drives at the last detected drives list
            foreach (DeviceInfoBO newDevice in newDetectedDevices)
            {
                Console.WriteLine("Device : " + newDevice.ID);

                if (ContainsDevice(lastDetectedDevices, newDevice) == false)
                {
                    //Notifies the log Manager
                    this.logManager.addMessage("New Drive " + newDevice.ID + " detected!");

                    //Exec. the associated eventHandler
                    if (this.newDeviceDetectedEvent != null)
                    {
                        //@TODO deja de buscar cuando va a hacer un backup, no deberia funcionar asi, cambiar cuando el multithread
                        this.searcherTimer.Stop();
                        
                        lastDetectedDevices = newDetectedDevices;
                        this.newDeviceDetectedEvent(newDevice);

                        //@TODO cambiar tambien
                        this.searcherTimer.Start();
                    }
                }
            }

            //Updates the last detected drives list
            lastDetectedDevices = newDetectedDevices;
        }

        private bool ContainsDevice(List<DeviceInfoBO> deviceList, DeviceInfoBO device)
        {
            bool contains = false;

            foreach (DeviceInfoBO d in deviceList)
            {
                if (d.ID == device.ID)
                {
                    contains = true;
                }
            }

            return (contains);
        }
    }
}
