﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using USBBackupManagerModel.DAL;

namespace USBBackupManager.BAL
{
    public class LogManagerBAL
    {
        public LogManagerBAL()
        {

        }

        public void addMessage(string message)
        {
            LogManagerDAL logManager = new LogManagerDAL();

            logManager.addMessage(message);
        }

        public void addHandler(LogManagerDAL.NewMessageLogHandler handler)
        {
            LogManagerDAL logManager = new LogManagerDAL();

            logManager.addHandler(handler);
        }

    }
}
