﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using USBBackupManager.BAL.Preferences;
using USBBackupManagerModel.BO.Preferences;
using USBBackupManagerModel.BO.Preferences.Exclusions;
using USBBackupManagerModel.BO.Preferences.Exclusions.Rules;

namespace USBBackupManager
{
    public partial class ExclusionsForm : Form
    {
        PreferencesManagerBAL preferencesManager;
        PreferencesBO preferences;

        public ExclusionsForm()
        {
            InitializeComponent();
        }

        private void ExclusionsForm_Load(object sender, EventArgs e)
        {
            //Load preferences
            this.preferencesManager = new PreferencesManagerBAL();
            this.preferences = preferencesManager.loadPreferences();

            //Set properties
            this.load(this.exclusionsDataGridView);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            this.save();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            this.save();
        }

        private void save()
        {
            //Clear actual rule list
            this.preferences.Exclusions.Rules.Clear();

            //Add a new rule for every datagrid row
            for (int i = 0; i < this.exclusionsDataGridView.Rows.Count-1; i++)
            {
                DataGridViewRow row = this.exclusionsDataGridView.Rows[i];
                RuleBO rule = new RuleBO(Convert.ToBoolean(row.Cells[0].Value),
                            Convert.ToBoolean(row.Cells[1].Value),
                            Convert.ToString(row.Cells[2].Value),
                            Convert.ToString(row.Cells[3].Value),
                            Convert.ToString(row.Cells[4].Value),
                            Convert.ToString(row.Cells[5].Value),
                            Convert.ToString(row.Cells[6].Value));

                this.preferences.Exclusions.addRule(rule);
            }

            //Saving
            this.preferencesManager.savePreferences(this.preferences);

            Console.WriteLine("Exclusions saved!");
        }

        private void load(DataGridView dataGrid)
        {
            //Insert a row in the dataGrid for each rule at exclusions.rules list
            foreach (RuleBO rule in this.preferences.Exclusions.Rules)
            {
                dataGrid.Rows.Add(rule.CopyAnyways,
                                    rule.Exclude,
                                    rule.Conditions[0].ConditionValue,
                                    rule.Conditions[1].ConditionValue,
                                    rule.Conditions[2].ConditionValue,
                                    rule.Conditions[3].ConditionValue,
                                    rule.Conditions[4].ConditionValue);
            }
        }

    }
}
