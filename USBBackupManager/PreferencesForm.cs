﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using USBBackupManagerModel.BO.Preferences;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Drive;
using USBBackupManager.BAL.Preferences;

using USBBackupManager.BAL;

namespace USBBackupManager
{
    public partial class PreferencesForm : Form
    {
        PreferencesManagerBAL preferencesManager;
        PreferencesBO preferences;

        public PreferencesForm()
        {
            InitializeComponent();
        }

        private void PreferencesForm_Load(object sender, EventArgs e)
        {
            //Load preferences
            this.preferencesManager = new PreferencesManagerBAL();
            this.preferences = preferencesManager.loadPreferences();

            //Set properties
            this.backupFolderTextBox.Text = preferences.BackupDirectory.FullURL;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                //Update the UI
                this.backupFolderTextBox.Text = this.folderBrowserDialog.SelectedPath;

                //Save preferences
                DeviceManagerBAL deviceManager = new DeviceManagerBAL();

                //Set & save
                preferences.BackupDirectory = new WSDirectoryInfoBO(this.folderBrowserDialog.SelectedPath);
                preferencesManager.savePreferences(preferences);

                Console.WriteLine("Opciones establecidas: " + preferences.BackupDirectory.FullURL);
            }


        }
    }
}
