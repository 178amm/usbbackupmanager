﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using USBBackupManagerModel.BO.Drive;
using USBBackupManagerModel.BO.Device;
using USBBackupManagerModel.BO.Directory;
using USBBackupManagerModel.BO.File;

using USBBackupManagerModel.BO.Preferences.Exclusions.Rules;
using USBBackupManagerModel.BO.Preferences.Exclusions;

using USBBackupManager.BAL;
using USBBackupManager.BAL.Preferences;
using USBBackupManagerModel.BO.Preferences;
using USBBackupManagerModel.Utils.States;
using USBBackupManager.BAL.Dump;

namespace USBBackupManager
{
    public partial class MainForm : Form
    {
        //BAL variables
        private DeviceMonitorBAL deviceMonitor;
        private DeviceManagerBAL deviceManager;
        private LogManagerBAL logManager;

        //BO variables
        private List<DriveInfoBO> driveBOList;

        //Tray variables
        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;

        public MainForm()
        {
            InitializeComponent();

            //Minimizes the app and shows the tray icon
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;

            //Init. BO
            this.deviceManager = new DeviceManagerBAL();
            this.deviceMonitor = new DeviceMonitorBAL();
            this.logManager    = new LogManagerBAL();
      
            //Init log handling
            this.logManager.addHandler(writeOnLiveLog);
            this.deviceMonitor.NewDeviceDetected += new DeviceMonitorBAL.NewDeviceDetectedEventHandler(OnNewDetectedDevice);

            //Create a simple tray menu
            trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Reset", dumper_Reset);
            trayMenu.MenuItems.Add("Stop", dumper_Stop);
            trayMenu.MenuItems.Add("Exit", dumper_Exit);

            //Set tray icon
            trayIcon = new NotifyIcon();
            trayIcon.Text = "JoyStickPadManager";
            trayIcon.Icon = new Icon(SystemIcons.Application, 40, 40); //Default icon
            trayIcon.DoubleClick += new EventHandler(trayIcon_DoubleClick);

            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
        }
        
        //UI Status management
        private State manualBackupMode;
        private State autoBackupMode;
        private StateMachine backupMode;

        private State busyBackupState;
        private State freeBackupState;
        private StateMachine backupState;

        private void setAutoBackupMode()
        {
            this.DrivesComboBox.Enabled         = false;
            this.GoButton.Enabled               = false;
            this.RefreshDrivesButton.Enabled    = false;
            this.label2.Enabled                 = false;
        }

        private void setManualBackupMode()
        {
            this.DrivesComboBox.Enabled         = true;
            this.GoButton.Enabled               = true;
            this.RefreshDrivesButton.Enabled    = true;
            this.label2.Enabled                 = true;
        }

        private void setBusyBackupState()
        {
            this.AbortButton.Enabled = true;
        }

        private void setFreeBackupState()
        {
            this.AbortButton.Enabled = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Init state machines

            //BACKUP MODE state machine, defines the type of backup system
            //  Auto: Wait in background for conected devices, when a device is detected, automatically creates a backup
            //  Manual: Allows the user to start a backup manually
            this.autoBackupMode     = new State("Auto", this.setAutoBackupMode);
            this.manualBackupMode   = new State("Manual", this.setManualBackupMode);
            this.autoBackupMode.addTrigger(manualBackupMode);
            this.manualBackupMode.addTrigger(autoBackupMode);

            this.backupMode = new StateMachine(new List<State> {this.manualBackupMode, this.autoBackupMode});

            //BACKUP STATUS state machine, defines if the program is currently performing a backup or not
            // Busy: Performing a backup
            // Free: Not performing a backup
            this.busyBackupState    = new State("Busy", this.setBusyBackupState);
            this.freeBackupState    = new State("Free", this.setFreeBackupState);
            this.busyBackupState.addTrigger(freeBackupState);
            this.freeBackupState.addTrigger(busyBackupState);

            //TODO rename backupState to dumper state
            this.backupState = new StateMachine(new List<State> {this.freeBackupState, this.busyBackupState});


            //Set default states
            this.backupMode.changeState(this.manualBackupMode);
            this.backupState.changeState(this.freeBackupState);

            //Refresh the drive list
            this.RefreshDrivesButton_Click(this, new EventArgs());

            //Set autoBackup on start
            this.AutoBackupModeCheckBox.Checked = true;

            this.logManager.addMessage("Welcome to Backup Manager");
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            //Tries to backup the selected drive on the comboBox list
            try
            {
                //Retrieves the drive
                DriveInfoBO drive = this.driveBOList[this.DrivesComboBox.SelectedIndex];

                //Loads the dumer & settings
                DumperBAL dumper = new DumperBAL();
                PreferencesManagerBAL preferencesManager = new PreferencesManagerBAL();
                PreferencesBO preferences = preferencesManager.loadPreferences();

                //Dumps the selected drive
                dumper.dumpDriveTo(drive, preferences.BackupDirectory, preferences.Exclusions);
            }
            catch (Exception ex)
            {
                this.logManager.addMessage(ex.Message.ToString());
                this.RefreshDrivesButton_Click(null, null);
            }

            //this.TEst();
        }

        private void RefreshDrivesButton_Click(object sender, EventArgs e)
        {
            //Updates the drive list retrieving all conected drives
            this.driveBOList = this.deviceManager.getConnectedDrives();
            this.DrivesComboBox.Items.Clear();

            //Load the drive list into the comboBox
            foreach (DriveInfoBO d in this.deviceManager.getConnectedDrives())
            {
                this.DrivesComboBox.Items.Add(d.DeviceRoot.Name + " [" + d.Root + "]");
            }

            //Updates the comboBox view
            this.DrivesComboBox.SelectedItem = this.DrivesComboBox.Items[0];
        }

        private void DrivesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AutoBackupModeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                this.backupMode.changeState(this.autoBackupMode);
                this.deviceMonitor.Start();
            }
            else
            {
                this.backupMode.changeState(this.manualBackupMode);
                this.deviceMonitor.Stop();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void writeOnLiveLog(string message)
        {
            if (InvokeRequired)
            {
                Invoke((Action<string>)writeOnLiveLog, message);
                return;
            }
                    
            this.liveLogListBox.Items.Add(message);
        }

        private void TEst()
        {
            //List<DeviceInfoBO> deviceList = this.deviceManager.getConnectedDevices();

            //DirectoryInfoBO directoryInfo = new WSDirectoryInfoBO((WSDriveInfoBO)deviceList[0].getAssociatedDrives()[0], "MyDumps");

            //USBBackupManager.BAL.Dump.DumperBAL dumper = new BAL.Dump.DumperBAL();

            //dumper.dumpDriveTo((WPDriveInfoBO)deviceList[1].getAssociatedDrives()[1], directoryInfo);

            RuleBO rule1 = new RuleBO(false, true, "", "", "", "", "na");
            RuleBO rule2 = new RuleBO(true, false, "", "", "", "", "na");
            //RuleBO rule3 = new RuleBO(true, false, "35", "66", "txt", "nbmbnncv", "as");
            ExclusionsBO exclusions = new ExclusionsBO();
            exclusions.addRule(rule1);
            exclusions.addRule(rule2);
            //exclusions.addRule(rule3);

            DummyFileBO file1 = new DummyFileBO(2000, "name.txt", "txt");

            //string stringrules = exclusions.getRules();
            Console.WriteLine("Should be included? : " + exclusions.ShouldBeIncluded(file1));

            //ExclusionsBO exclusions2 = new ExclusionsBO();
            //exclusions2.setRules(stringrules);
            //string stringrules2 = exclusions2.getRules();

            //DirectoryInfoBO dir = new WSDirectoryInfoBO("C:/Users/dadrimon2/Desktop/dumps");

            //Console.WriteLine("DIRECTORYURL: " + Path.GetFullPath(dir.DirectoryURL));
            //Console.WriteLine("DIRECTORYID: " + dir.ID);
            //Console.WriteLine("DIRECTORYNAME: " + dir.Name);
            //Console.WriteLine("FULLURL: " + Path.GetFullPath(dir.FullURL));

        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (PreferencesForm preferencesForm = new PreferencesForm())
            {
                DialogResult dialogResult = preferencesForm.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {

                }
            }
        }

        private void exclusionsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (ExclusionsForm exclusionsForm = new ExclusionsForm())
            {
                DialogResult dialogResult = exclusionsForm.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {

                }
            }
        }

        public void OnNewDetectedDevice(DeviceInfoBO detectedDevice)
        {
            DumperBAL dumper = new DumperBAL();
            PreferencesManagerBAL preferencesManager = new PreferencesManagerBAL();
            PreferencesBO preferences = preferencesManager.loadPreferences();

            foreach (DriveInfoBO drive in detectedDevice.getAssociatedDrives())
            {
                Console.WriteLine("New detected drive: " + drive.DirectoryRoot.FullURL);
                Console.WriteLine("Preferences backupDirectory: " + preferences.BackupDirectory.FullURL);
                dumper.dumpDriveTo(drive, preferences.BackupDirectory, preferences.Exclusions);
            }
        }

        private void onClosing(object sender, FormClosingEventArgs e)
        {
            //Minimizes the app and shows the tray icon
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.trayIcon.Visible = true;

            //Prevents from real closing
            e.Cancel = true;
        }

        #region Tray Event Handlers

        //Double click on the icon
        void trayIcon_DoubleClick(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
            this.WindowState = FormWindowState.Normal;
            this.trayIcon.Visible = false;
        }

        //Exit from the tray icon
        private void dumper_Exit(object sender, EventArgs e)
        {
            this.trayIcon.Dispose();
            Application.Exit();
            Environment.Exit(0);
        }

        //Stop from the tray icon
        private void dumper_Stop(object sender, EventArgs e)
        {
            //this.stopButton_Click(sender, e);
        }

        //Reset from the tray icon
        private void dumper_Reset(object sender, EventArgs e)
        {
            //this.stopButton_Click(sender, e);
            //dumper.DumpLastDrive();
        }

        #endregion

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.trayIcon.Dispose();
            Application.Exit();
            Environment.Exit(0);
        }
    }
}
